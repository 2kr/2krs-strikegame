import { Component, OnInit } from '@angular/core';
import { Record } from '../record';
import { RECORDS } from '../record-mok';
import { of } from 'rxjs';
import { Key } from 'protractor';
// import { isPlatformBrowser } from '@angular/common';

import { AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';


@Component({
  selector: 'app-strike',
  templateUrl: './strike.component.html',
  styleUrls: ['./strike.component.css']
})
export class StrikeComponent implements OnInit {
  // records = RECORDS;

  test = '1';

  rrecord: string[] = [];

  startGame: string;
  state = 'start';

  first: string;
  second: string;
  third: string;
  createNumberArray: string[] = [];

  one: string;
  two: string;
  three: string;

  strikeCount: number;
  ballCount: number;
  inning = 0;
  result = false;

  name: string;

  items: Observable<any[]>;
  constructor(public db: AngularFirestore) {
    this.items = db.collection('users').valueChanges();
  }

  ngOnInit() {
  }

  // 랜덤숫자 생성
  start() {
    this.result = false;
    this.rrecord = [];
    this.one = '';
    this.two = '';
    this.three = '';
    this.inning = 0;
    this.startGame = 'start';
    this.state = 'restart';

    const numberArray: string[] = ['1', '2', '3', '4', '5', '6', '7', '8', '9'];

    const index1 = Math.round(Math.random() * 8);
    const index2 = Math.round(Math.random() * 7);
    const index3 = Math.round(Math.random() * 6);

    this.first = numberArray[index1];
    numberArray.splice(index1, 1);
    this.second = numberArray[index2];
    numberArray.splice(index2, 1);
    this.third = numberArray[index3];
    this.createNumberArray = [this.first, this.second, this.third];
    console.log(this.createNumberArray);
  }
  // 숫자비교
  compareNumber() {
    this.strikeCount = 0;
    this.ballCount = 0;
    this.inning += 1;
    const inputArray: string[] = [this.one, this.two, this.three];

    this.createNumberArray.forEach((value1, idx1) => {
      inputArray.forEach((value2, idx2) => {
        if (value1 === value2) {
          if (idx1 === idx2) {
            this.strikeCount += 1;
          } else {
            this.ballCount += 1;
          }
        } else {

        }
      });
    });

    this.rrecord.push(this.one + this.two + this.three + "-->" +this.strikeCount + "-strike" + this.ballCount + "-ball");

    if (this.strikeCount === 3) {
      this.result = true;
    }
    this.one = '';
    this.two = '';
    this.three = '';
  }

  setFocus1(move) {
  const test: number = Number(this.one);
  if (Number.isInteger(test)) {
    move.focus();
   } else {
     alert('정수만입력가능합니다');
     this.one = '';
   }
  }
  setFocus2(move) {
    const test: number = Number(this.two);
    if (Number.isInteger(test)) {
      // move.removeAttr('readonly');
      move.focus();
     } else {
       alert('정수만입력가능합니다');
       this.two = '';
     }
  }
  setFocus3(move) {
    const test: number = Number(this.three);
    if (Number.isInteger(test)) {
       this.compareNumber();
       move.focus();
     } else {
       alert('정수만입력가능합니다');
       this.three = '';
     }
  }

  savedb() {
    alert('저장되었습니다');

    const query = this.db.collection('users').add({
      name : this.name,
      inning : this.inning
    });
    return query;
  }

}
