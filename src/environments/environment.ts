// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyASa3CAggRKVur-3BCUjbJBWnb93B-qpx0",
    authDomain: "firstproject-3c350.firebaseapp.com",
    databaseURL: "https://firstproject-3c350.firebaseio.com",
    projectId: "firstproject-3c350",
    storageBucket: "firstproject-3c350.appspot.com",
    messagingSenderId: "786497150170"
  }
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
